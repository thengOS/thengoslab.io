var dataCacheName = 'തെങ്ങ്ഒഎസ്';
var cacheName = 'തെങ്ങ്ഒഎസ്';
var filesToCache = [
  '/',
  '/index.html',
  '/assets/css/images/overlay.png',
  '/assets/css/images/pattern-size1.svg',
  '/assets/css/images/pattern-size2.svg',
  '/assets/css/images/pattern-size3.svg',
  '/assets/css/font-awesome.min.css',
  '/assets/css/main.css',
  '/assets/css/noscript.css',
  '/assets/fonts/FontAwesome/fontawesome-webfont.eot',
  '/assets/fonts/FontAwesome/fontawesome-webfont.svg',
  '/assets/fonts/FontAwesome/fontawesome-webfont.ttf',
  '/assets/fonts/FontAwesome/fontawesome-webfont.woff',
  '/assets/fonts/FontAwesome/fontawesome-webfont.woff2',
  '/assets/fonts/FontAwesome/FontAwesome.otf',
  '/assets/fonts/Manjari/Manjari-Bold.otf',
  '/assets/fonts/Manjari/Manjari-Bold.ttf',
  '/assets/fonts/Manjari/Manjari-Bold.woff',
  '/assets/fonts/Manjari/Manjari-Bold.woff2',
  '/assets/fonts/Manjari/Manjari-Regular.otf',
  '/assets/fonts/Manjari/Manjari-Regular.ttf',
  '/assets/fonts/Manjari/Manjari-Regular.woff',
  '/assets/fonts/Manjari/Manjari-Regular.woff2',
  '/assets/fonts/Manjari/Manjari-Thin.otf',
  '/assets/fonts/Manjari/Manjari-Thin.ttf',
  '/assets/fonts/Manjari/Manjari-Thin.woff',
  '/assets/js/breakpoints.min.js',
  '/assets/js/browser.min.js',
  '/assets/js/jquery.min.js',
  '/assets/js/jquery.scrolly.min.js',
  '/assets/js/main.js',
  '/assets/js/util.js',
  '/images/desktop.png',
  '/images/favicon.ico',
  '/images/pic01.jpg',
  '/images/pic02.jpg',
  '/images/pic03.jpg',
  '/images/icons/icon-128x128.png',
  '/images/icons/icon-144x144.png',
  '/images/icons/icon-152x152.png',
  '/images/icons/icon-192x192.png',
  '/images/icons/icon-384x384.png',
  '/images/icons/icon-512x512.png',
  '/images/icons/icon-72x72.png',
  '/images/icons/icon-96x96.png',
  '/manifest.json'
];

self.addEventListener('install', function(e) {
  console.log('[ServiceWorker] Install');
  e.waitUntil(
    caches.open(cacheName).then(function(cache) {
      console.log('[ServiceWorker] Caching app shell');
      return cache.addAll(filesToCache);
    })
  );
});

self.addEventListener('activate', function(e) {
  console.log('[ServiceWorker] Activate');
  e.waitUntil(
    caches.keys().then(function(keyList) {
      return Promise.all(keyList.map(function(key) {
        if (key !== cacheName && key !== dataCacheName) {
          console.log('[ServiceWorker] Removing old cache', key);
          return caches.delete(key);
        }
      }));
    })
  );
 
    /*
     * Fixes a corner case in which the app wasn't returning the latest data.
     * You can reproduce the corner case by commenting out the line below and
     * then doing the following steps: 1) load app for first time so that the
     * initial New York City data is shown 2) press the refresh button on the
     * app 3) go offline 4) reload the app. You expect to see the newer NYC
     * data, but you actually see the initial data. This happens because the
     * service worker is not yet activated. The code below essentially lets
     * you activate the service worker faster.
     */
    return self.clients.claim();
  });
  this.addEventListener('fetch', function (event) {
    // it can be empty if you just want to get rid of that error
});